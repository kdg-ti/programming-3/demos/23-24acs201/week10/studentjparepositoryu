package be.kdg.programming3.studentjparepository.domain;

import jakarta.persistence.*;

import java.time.LocalDate;

@MappedSuperclass
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected
	int id;
	protected String name;
	protected double length;
	protected LocalDate birthday;
}
