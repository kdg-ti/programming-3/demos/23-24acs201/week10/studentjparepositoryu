package be.kdg.programming3.studentjparepository.domain;

import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
public class AcsStudent extends Student{
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	String country;

	public AcsStudent(String country) {
		this.country = country;
	}

	public AcsStudent(String name,
		double length,
		LocalDate birthday,
		int credits,
		String country) {
		super(name, length, birthday, credits);
		this.country = country;
	}

	public AcsStudent() {
	}
}
