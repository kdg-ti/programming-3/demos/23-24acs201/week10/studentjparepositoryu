package be.kdg.programming3.studentjparepository.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name = "STUDENTS")
@Inheritance(strategy = InheritanceType.JOINED)
public class Student extends Person {
    private int credits;

	@OneToOne(cascade = CascadeType.ALL)
	private Address address;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Course> courses = new ArrayList<>();

	protected Student() {
	}

	public static Student getRandom() {
		Random random = new Random();
		Student student = new Student("name" + random.nextInt(1000),
			random.nextDouble(130, 210), LocalDate.of(random.nextInt(1900, 2010),
			random.nextInt(1, 13), random.nextInt(1, 28)), random.nextInt(0, 180),
			Address.getRandom());
		for (int i = 0; i < random.nextInt(1, 5); i++) {
			student.addCourse(Course.getRandom());
		}
		return student;
	}

    public Student(String name, double length, LocalDate birthday, int credits) {
        this.name = name;
        this.length = length;
        this.birthday = birthday;
        this.credits = credits;
    }

    	public Student(String name, double length, LocalDate birthday, int credits, Address address) {
		this.name = name;
		this.length = length;
		this.birthday = birthday;
		this.credits = credits;
		this.address = address;
	}

  	public void addCourse(Course course) {
		courses.add(course);
	}

	public void removeCourse(Course course) {
		courses.remove(course);
	}

    	@Override
	public String toString() {
		return "Student{" +
			"id=" + id +
			", name='" + name + '\'' +
			", length=" + length +
			", birthday=" + birthday +
			", credits=" + credits +
			", address=" + address +
			'}';
	}

	public List<Course> getCourses() {
		return courses;
	}
}
