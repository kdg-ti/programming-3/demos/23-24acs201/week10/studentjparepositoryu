package be.kdg.programming3.studentjparepository.domain;

import jakarta.persistence.*;

import java.util.Random;

@Entity
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int addressId;
	private String street;
	private int postalCode;
	private String city;


	protected Address() {
	}

	public Address(String street, int postalCode, String city) {
		this.street = street;
		this.postalCode = postalCode;
		this.city = city;
	}

	public static Address getRandom() {
		Random random = new Random();
		String[] streets = {"Nationale Straat",  "LombardenVest", "Pothoekstraat", "Predikerinnenstraat"};
		String[] cities = {"Antwerp", "Erps Kwerps", "Kieldrecht", "NederOverHeembeek", "Bouge"};
		return new Address(streets[random.nextInt(streets.length)] + " " + random.nextInt(1, 200),
			random.nextInt(100, 1000)*10,
			cities[random.nextInt(cities.length)]);
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}


	@Override
	public String toString() {
		return "Adres{" +
			", street='" + street + '\'' +
			", postalCode=" + postalCode +
			", city='" + city + '\'' +
			'}';
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int id) {
		this.addressId = id;
	}
}
