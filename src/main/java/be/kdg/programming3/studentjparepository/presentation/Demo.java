package be.kdg.programming3.studentjparepository.presentation;

import be.kdg.programming3.studentjparepository.domain.AcsStudent;
import be.kdg.programming3.studentjparepository.domain.Student;
import be.kdg.programming3.studentjparepository.service.StudentService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Scanner;

@Component
public class Demo implements CommandLineRunner {

	StudentService studentService;

	public Demo(StudentService studentService) {
		this.studentService = studentService;
	}

	@Override
	public void run(String... args) throws Exception {
		for (int count = 0; count < 100; count++) {
			studentService.addStudent(Student.getRandom());
		}
		System.out.println("Created: " + studentService.findStudents());
		printStudent(1);
		printStudentWithCourses(1);
		printStudent(1000);
		System.out.println(">>> Pothoek students " + studentService.findStudentsInStreet("Pothoek"));
		System.out.println(">>> Big students " + studentService.findBigStudents());
		System.out.println(">>> Small students " + studentService.findSmallStudents());
		System.out.println(">>> Old students " + studentService.findOldStudents());
		System.out.println(">>> Young students " + studentService.findYoungStudents());
		System.out.println(">>> Tenfolds " + studentService.findTenFolds());
		System.out.println(">>> Programming ");
		for (Student student : studentService.findStudentsByCourseName("Programming")){
			System.out.println(student + student.getCourses().toString());
		};
//    Scanner scanner = new Scanner(System.in);
//		scanner.nextLine();
	System.out.println(">>> Duplicates " + studentService.findStudentsWithDuplicateNames());


		System.out.println(studentService.addStudent(new AcsStudent("peter",
			210,
			LocalDate.of(2000, 1, 1),
			60,
			"UK")));
	}

	private void printStudent(int id) {
		Optional<Student> student = studentService.findStudent(id);
		System.out.println(student.isPresent() ?
			student.get() :
			"Student " + id + " not found");
	}

	private void printStudentWithCourses(int id) {
		Optional<Student> student = studentService.findStudentAndCourses(id);
		System.out.println(student.isPresent() ?
			student.get() + student.get().getCourses().toString() :
			"Student " + id + " not found");
	}
}
