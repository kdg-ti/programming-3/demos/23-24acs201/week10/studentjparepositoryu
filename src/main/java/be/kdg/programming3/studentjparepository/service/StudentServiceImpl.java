package be.kdg.programming3.studentjparepository.service;

import be.kdg.programming3.studentjparepository.domain.Course;
import be.kdg.programming3.studentjparepository.domain.Student;
import be.kdg.programming3.studentjparepository.repository.StudentRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
public class StudentServiceImpl implements StudentService {

	StudentRepository studentRepository;

	public StudentServiceImpl(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	public Student addStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public List<Student> findStudents() {
		return studentRepository.findAll();
	}

	@Override
	public Optional<Student> findStudent(int id) {
		return studentRepository.findById(id);
	}

	@Override
	public Optional<Student> findStudentAndCourses(int id) {
		return studentRepository.findByIdWithCourses(id);
	}

	@Override
	public void changeStudent(Student student) {

	}

	@Override
	public void deleteStudent(int id) {

	}

	@Override
	public List<Student> findBigStudents() {
		return studentRepository.findByLengthGreaterThan(200);
	}

	@Override
	public List<Student> findSmallStudents() {
		return studentRepository.findByLengthLessThan(140);
	}

	@Override
	public List<Student> findOldStudents() {
		return studentRepository.findByBirthdayBefore((LocalDate.now().minusYears(80)));
	}

	@Override
	public List<Student> findYoungStudents() {
		return studentRepository.findByBirthdayAfter(LocalDate.now().minusYears(30));
	}

	@Override
	public List<Student> findStudentsInStreet(String street) {
		return studentRepository.findByAddressStreetStartsWith(street);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Student> findStudentsByCourseName(String courseName) {
		List<Student> students = studentRepository.findByCoursesName(courseName);
		// lazy loading in service
		for (Student student : students) {
			for (Course course:student.getCourses()){

			}
		}
		return students;
	}

	@Override
	public List<Student> findTenFolds() {
		return studentRepository.findByNameLike("%0", PageRequest.of(0, 3));
	}

	@Override
	public List<Student> findStudentsWithDuplicateNames() {
		return studentRepository.findStudentsWithDuplicateNames();
	}

}
