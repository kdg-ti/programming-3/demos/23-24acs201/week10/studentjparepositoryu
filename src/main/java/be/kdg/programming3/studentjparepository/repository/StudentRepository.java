package be.kdg.programming3.studentjparepository.repository;

import be.kdg.programming3.studentjparepository.domain.Student;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
	List<Student> findByLengthGreaterThan(double length);

	List<Student> findByLengthLessThan(double length);

	List<Student> findByBirthdayBefore(LocalDate date);

	List<Student> findByBirthdayAfter(LocalDate date);

	@Query("SELECT s FROM Student s WHERE s.address.street LIKE :street%")
		List<Student> findByAddressStreetStartsWith(String street);


	// works both with and withour @Query
	@Query("SELECT s FROM Student s WHERE s.name LIKE :suffix")
	List<Student> findByNameLike(String suffix, Pageable paging);

//    @Query(value = """
//	    SELECT s.name FROM students s WHERE s.name IN
//            (SELECT name from students  group by name
//            HAVING COUNT(name) > 1 )
//	    	ORDER BY s.name
//	   """, nativeQuery = true)
//	List<String> findStudentsWithDuplicateNames();

	    @Query(value = """
	    SELECT s FROM Student s WHERE s.name IN
            (SELECT s2.name from Student s2 group by s2.name
            HAVING COUNT(s2.name) > 1 )
	    	ORDER BY s.name
	   """)
	List<Student> findStudentsWithDuplicateNames();

	@Query("""
		SELECT s 
		FROM Student s 
		JOIN FETCH s.courses 
		WHERE s.id = :id
		""")
	Optional<Student> findByIdWithCourses(int id);

	List<Student> findByCoursesName(String name);


}
